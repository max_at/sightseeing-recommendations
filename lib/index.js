const fse = require('fs-extra');
const path = require('path');
const ejs = require('ejs');
const marked = require('marked');
const frontMatter = require('front-matter');
const glob = require('glob');
const chokidar = require('chokidar');
const debounce = require('lodash.debounce');
const server = require('./utils/server');
const log = require('./utils/logger');
const csv = require('fast-csv');

const buildDefaults = { srcPath: './src', outputPath: './public' };

/**
 * read csv
 */

let csvData = {};
const build = (options = {}) => {
  console.log('Verarbeite Sightseeing CSV: ', options.site.file);
  csvData = {};
  const stream = fse.createReadStream(__dirname + '/../site/csv/' + options.site.file + '.csv');
  var csvStream = csv({
    trim: true,
    headers: true,
    ignoreEmpty: true,
    delimiter: ';'
  })
    .on("data", function (d) {
      if (!d.Category) {
        return;
      }
      // csvData.push(data);
      if (!csvData[d.Country]) {
        csvData[d.Country] = {};
      }
      if (!csvData[d.Country][d.Category]) {
        csvData[d.Country][d.Category] = [];
      }
      csvData[d.Country][d.Category].push(d);
    })
    .on("end", function () {
      console.log("Fertig.");
      // console.log(JSON.stringify(csvData['Germany']['Castles'][0], null, 2));
      buildNow(options);
    });

  stream.pipe(csvStream);
};

/**
 * Parse options, setting the defaults on missing values
 */
const _parseOptions = options => {
  const { srcPath, outputPath } = Object.assign(
    {},
    buildDefaults,
    options.build
  );
  const site = options.site || {};

  return { srcPath, outputPath, site };
};

/**
 * Loads a layout file
 */
const _loadLayout = (layout, { srcPath }) => {
  const file = `${srcPath}/layouts/${layout}.ejs`;
  const data = fse.readFileSync(file, 'utf-8');

  return { file, data };
};

/**
 * Build a single page
 */
const _buildPage = (file, { srcPath, outputPath, site }) => {
  const fileData = path.parse(file);
  let destPath = path.join(outputPath, fileData.dir);

  // create extra dir if filename is not index
  if (fileData.name !== 'index') {
    destPath = path.join(destPath, fileData.name);
  }

  // create destination directory
  fse.mkdirsSync(destPath);

  // read page file
  const data = fse.readFileSync(`${srcPath}/pages/${file}`, 'utf-8');

  // render page
  const pageData = frontMatter(data);
  const templateConfig = {
    site,
    page: pageData.attributes,
    csvData
  };

  let pageContent;
  const pageSlug = file.split(path.sep).join('-');

  // generate page content according to file type
  switch (fileData.ext) {
    case '.md':
      pageContent = marked(pageData.body);
      break;
    case '.ejs':
      pageContent = ejs.render(pageData.body, templateConfig, {
        filename: `${srcPath}/page-${pageSlug}`
      });
      break;
    default:
      pageContent = pageData.body;
  }

  // render layout with page contents
  const layoutName = pageData.attributes.layout || 'default';
  const layout = _loadLayout(layoutName, {
    srcPath
  });

  const completePage = ejs.render(
    layout.data,
    Object.assign({}, templateConfig, {
      body: pageContent,
      filename: `${srcPath}/layout-${layoutName}`,
      csvData: csvData
    })
  );

  // save the html file
  fse.writeFileSync(`${destPath}/index.html`, completePage);
};

/**
 * Build the site
 */
const buildNow = (options = {}) => {
  log.info('Building Site...', csvData.length);

  const startTime = process.hrtime();
  const { srcPath, outputPath, site } = _parseOptions(options);

  // clear destination folder
  fse.emptyDirSync(outputPath);

  // copy assets folder
  if (fse.existsSync(`${srcPath}/assets`)) {
    fse.copySync(`${srcPath}/assets`, outputPath);
  }

  // read pages
  const files = glob.sync('**/*.@(md|ejs|html)', { cwd: `${srcPath}/pages` });

  files.forEach(file => _buildPage(file, { srcPath, outputPath, site }));

  // display build time
  const timeDiff = process.hrtime(startTime);
  const duration = timeDiff[0] * 1000 + timeDiff[1] / 1e6;
  log.success(`Finished after ${duration}ms`);
};

/**
 * Serve the site in watch mode
 */
const serve = (options, flags) => {
  log.info(`Starting local server at http://localhost:${flags.port}`);

  const { srcPath, outputPath } = _parseOptions(options);

  server.serve({ path: outputPath, port: flags.port });

  chokidar.watch(srcPath).on(
    'all',
    debounce(() => {
      build(options);
      log.info('Waiting for changes...', csvData.length);
    }, 500)
  );
};

module.exports = {
  build,
  serve
};
