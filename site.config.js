module.exports = {
  build: {
    srcPath: './site',
    outputPath: './output'
  },
  site: {
    title: 'Sightseeing Recommendations',
    description: '',
    basePath: process.env.NODE_ENV === 'production' ? '/nanogen' : ''
  }
};
