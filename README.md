# Sightseeing Recommendations

## Installation

Prerequisite: [Node.js](https://nodejs.org/en/download/) greater than version 6.

Run in the main folder:

    npm install

## Running

Run in the main folder one of the predefined country guides: 

    npm run start:germany
    npm run start:austria

Or 

    npm run start -- -f your-other-country.csv

## Workflow

Eg. for editing germany:  
  
Run the tool with `npm run start:germany` and open [http://localhost:3000](http://localhost:3000) in your browser.  
  
Open `site/csv/germany.csv` in Excel  
Add a new line with your desired category, headline, description, website link.  
Search for a picture on the web and save it under `site/assets/germany` and store its name in the Image column.
  
Reload the already opened browser and verify your changes. Show or hide the helpers for a better view on the changes.  
  
Open the print preview for optimization of page breaks. Follow the instructions in _optimizing page breaks_ section below 

## Folder structure

`site/csv` - contains the csv files that we are referencing (eg. germany.csv)  
`site/assets` - contains a folder for each csv, where it's specific files are located (sight images, header image)  
eg:  
`site/assets/germany/`  
Header image is fetched from that foler too:  
`site/assets/germany/header.jpg`  

### Optimizing page breaks

Modify `.file .category:nth-child()` part in site/css/styles.css  
in order to create space between certain category sections.  
Use it for moving a section to the next page when using print preview before saving it as pdf.
Helpers on the left side of the tool can be copied for easier finding the right nth-child.

## Advanced

If you need to change the layout itself:  
`site/pages/index.ejs`  
`site/css/styles.css`  

## Authors

* **Markus Edenhauser** - *CSV Extension, Template* - [maxmarkus](https://github.com/maxmarkus)

Based on [nanogen](https://github.com/doug2k1/nanogen)

## License

This project is licensed under the MIT License
